This program is intended to show metrics related to fitness over time, and to predict performance in the near future. This will be accomplished using data from periods of exercise.

Initially the program will use an extremely simple performance model: performance = fitness + form, where, following some exercise, fitness increases a small amount and then slowly decays and form decreases a large amount and quickly recovers.

Some features that I hope to include:
- accept data in a variety of formats (.gpx, .fit, etc.)
- include heart rate data
- include weather data
- collect this data from other services (e.g. Strava)
- provide a web interface to plots/data
- identify correlations with other metrics (e.g. optimal performance is achieved when you drink 1 l of water per hour)
- a nice GUI
- suggest training plans (more difficult).